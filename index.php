<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/index.css">
        <title>Formulario de Votación</title>
    </head>
    <body>
        <h2>FORMULARIO DE VOTACIÓN</h2>
        <br/>
        <form>
            <table border="0px" >
                <tr>
                    <td align="left" ><label class="formulario">Nombre y apellido:&nbsp&nbsp &nbsp&nbsp</label><br/></td>
                    <td><input type="text" name="nombre" id="nombre" placeholder="Ejemplo: Andrés Salas" onkeypress="return soloLetrasNombre(event)"></td>
                </tr>               
                <tr>
                    <td align="left" ><br/><label class="formulario">Alias:</label></td>
                    <td><br/><input type="text" name="alias" id="alias" placeholder="Ejemplo: andres55" onkeypress="return soloNumerosLetras(event)"></td>
                </tr>                
                <tr>
                    <td align="left" ><br/><label class="formulario">RUT:</label></td>
                    <td><br/><input type="text" name="rut" id="rut" placeholder="Ejemplo: 1111111-1" onkeypress="return soloNumerosRut(event)" maxlength="10"></td>
                </tr>
                <tr>
                    <td align="left" ><br/><label class="formulario">Email:</label></td>
                    <td><br/><input type="email" name="email" id="email" placeholder="Ejemplo: algo@mail.com" onkeypress="return ingresoCorreoElectronico(event)"></td>
                </tr>
                <tr>
                    <td align="left" ><br/><label class="formulario">Región:</label></td>
                    <td><br/><select id="region" name="region" class="form-control"></select></td>
                </tr>
                <tr>
                    <td align="left" ><br/><label class="formulario">Comuna:</label></td>
                    <td><br/><select id="ciudad" name="ciudad" class="form-control"></select></td>
                </tr>
                <tr>
                    <td align="left" ><br/><label class="formulario">Candidato:</label></td>
                    <td><br><select id="candidato" name="candidato" class="form-control"></select></td>                
                </tr>
            </table>
            <br/><label class="formulario">Como se enteró de nosotros:&nbsp&nbsp</label>
            <input type="checkbox" name="web" id="web"><label class="formulario-check"> Web&nbsp&nbsp</label>
            <input type="checkbox" name="TV" id="TV"><label class="formulario-check"> TV&nbsp&nbsp</label>
            <input type="checkbox" name="rs" id="rs"><label class="formulario-check"> Redes Sociales&nbsp&nbsp</label>
            <input type="checkbox" name="amigo" id="amigo"><label class="formulario-check"> Amigo</label>        
            <br/>
            <input type="button" value="Votar" onclick="operacion();"><br/>  
        </form>      
        <div id="divResultado" style="display: none;">
            <br>
            <label id="labelResultado"></label><br><br>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="js/validador.js"></script>
        <script type="text/javascript" src="js/index.js"></script> 
        <script type="text/javascript" src="js/combobox.js"></script>   
    </body>
</html>