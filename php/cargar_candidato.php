<?php
require 'conexion.php';

function getCandidato()
{
    $pgsql = getConn();
    $query = "SELECT * FROM candidato";
    $result = pg_query($pgsql, $query) or die('Falló la query: ' . pg_last_error());;
    $listas = '<option value="0">Elige una opción</option>';
    while($row = pg_fetch_object($result))
    {
        $listas = $listas."<option value='$row->id'>$row->nombre</option>";
    }
    return $listas;
};

echo getCandidato();