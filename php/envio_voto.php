<?php
require 'conexion.php';

function setVoto()
{
    $pgsql = getConn();
    //Llamada a todas las variables desde el AJAX
    $nombre= pg_escape_string($_POST['nombre']);
    $alias= pg_escape_string($_POST['alias']);
    $rut= pg_escape_string($_POST['rut']);
    $email= pg_escape_string($_POST['email']);
    $idregion= $_POST['region'];
    $idciudad= $_POST['ciudad'];
    $idcandidato= $_POST['candidato'];
    $web= $_POST['web'];
    $TV= $_POST['TV'];
    $rs= $_POST['rs'];
    $amigo= $_POST['amigo'];
    //NOTA: De esta forma, antes de hace run insert, el sistema guardará los id de los
    //candidatos, ciudades y regiones; esto se corregirá con 3 querys:

    $query_candidato = "SELECT nombre FROM candidato WHERE id=$idcandidato";
    $query_region = "SELECT nombre_region FROM region WHERE id_region=$idregion";
    $query_ciudad = "SELECT nombre_ciudad FROM ciudad WHERE id_ciudad=$idciudad";
    
    $candidato_ = pg_query($pgsql, $query_candidato) or die('Falló la query: ' . pg_last_error());
    $region_ = pg_query($pgsql, $query_region) or die('Falló la query: ' . pg_last_error());
    $ciudad_ = pg_query($pgsql, $query_ciudad) or die('Fallo la query: ' . pg_last_error());
    //Extracción del dato
    $candidato=pg_fetch_object($candidato_)->nombre; //Solo hay una ocurrencia!
    $region=pg_fetch_object($region_)->nombre_region; //Solo hay una ocurrencia!
    $ciudad=pg_fetch_object($ciudad_)->nombre_ciudad; //Solo hay una ocurrencia!
    
    /*Nota: A nivel de DB se ha implementado esta política. Se deja esto a nivel de doble revisión*/
    //REVISIÓN 1: NO EXISTE DUPLICIDAD DE RUT
    $query_rut = "SELECT rut FROM votos WHERE rut='$rut'";
    if(pg_fetch_object(pg_query($pgsql, $query_rut))->rut == $rut)
        return 1;
    //REVISIÓN 2: NO EXISTE DUPLICIDAD DE ALIAS--X CORREGIR!
    $query_alias = "SELECT alias FROM votos WHERE alias='$alias'";
    if(pg_fetch_object(pg_query($pgsql, $query_alias))->alias == $alias)
        return 2;
    //Todo bien, se procede a insertar los datos
    $query_agregar="INSERT INTO votos (nombre, alias, rut, email, region, ciudad, candidato, 
    web, tv, rs, amigo) VALUES ('$nombre','$alias','$rut','$email','$region','$ciudad',
    '$candidato','$web','$TV','$rs','$amigo')";
    //AHORA, SE ENVIARÁN LOS DATOS A LA TABLA "votos"
    try
    {
        pg_query($pgsql, $query_agregar) or die('Falló la query: ' . pg_last_error());
        return 0;
    }
    catch(Exception $e)
    {
        echo $e->getMessage();
        return 3;
    };
};

echo setVoto();