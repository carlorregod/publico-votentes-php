<?php
require 'conexion.php';

function getRegion()
{
    $pgsql = getConn();
    $query = "SELECT * FROM region";
    $result = pg_query($pgsql, $query) or die('Falló la query: ' . pg_last_error());;
    $listas = '<option value="0">Elige una opción</option>';
    while($row = pg_fetch_object($result))
    {
        $listas = $listas."<option value='$row->id_region'>$row->nombre_region</option>";
    }
    return $listas;
};

echo getRegion();