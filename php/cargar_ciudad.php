<?php
require 'conexion.php';

function getCiudad()
{
    $pgsql = getConn();
    $id = $_POST['id'];
    $query = "SELECT * FROM ciudad WHERE id_region=$id";
    $result = pg_query($pgsql, $query) or die('Falló la query: ' . pg_last_error());;
    $listas = '<option value="0">Elige una opción</option>';
    while($row = pg_fetch_object($result))
    {
        $listas = $listas."<option value='$row->id_ciudad'>$row->nombre_ciudad</option>";
    }
    return $listas;
};

echo getCiudad();