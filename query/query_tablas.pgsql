/**Compendio de tablas y compleción de ellas para los combobox**/
--CREACION DE TABLAS Y ALGUNOS VALORES DE REFERENICIA

create table candidato(
    id serial not null primary key,
    nombre varchar not null
);

INSERT INTO candidato (nombre)
VALUES ('Juan P�rez'), ('Carlos Ortega'),('Jos� Orrego');

create table region(
    id_region serial not null primary key,
    nombre_region varchar not null
);

INSERT INTO region (nombre_region)
VALUES ('RM'), ('V Regi�n'),('VI Regi�n');

//NO SE USAR� TABLA CIUDAD PERO SE DEJAR� COMO RECURSO A FUTURO
create table comuna(
    id_comuna serial not null,
    nombre_comuna varchar not null,
    id_region int not null,
    primary key(id_comuna)
);
alter table ciudad
add constraint FK_region_ciudad
foreign key(id_region) 
references region(id_region);

insert into comuna(nombre_comuna, id_region)
values ('Macul',1),('San Miguel',1),('Santiago',1),
('Valparaiso',2),('Vina del Mar',2),('Quintero',2),
('Rancagua',3),('San Fernando',3),('Machali',3);

CREATE TABLE votos(
    id serial not null primary key,
    nombre varchar,
    alias varchar,
    rut varchar(12),
    email varchar,
    region varchar,
    comuna varchar,
    candidato varchar,
    web boolean,
    TV boolean,
    rs boolean,
    amigo boolean
);
--GARANTIZAR DATOS NO DUPLICADOS DE RUT Y/O ALIAS
alter table votos
   add constraint UQ_alias
   unique (alias);

alter table votos
   add constraint UQ_rut
   unique (rut);

--TESTEOS DE EJEMPLO
select * from ciudad;
select * from candidato;
select * from comuna;
select * from votos;