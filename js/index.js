/*PARTE 1: MÉTODOS Y FUNCIONES VARIAS*/
//Revisión de ingresos Alias, RUT y email. Si aceptan las condiciones específicas:
//Revisa el alias ingresado
function revisaAlias(alias)
{
    //Consultar si el caracter tiene dígitos
    var regex_num = /\d+/;
    //Consultar si el caracter tiene letras
    var regex_txt= /([a-zAZ])+/;
    if(alias.search(regex_num) == -1 || alias.search(regex_txt) == -1)
    {
        alert("Alias debe poseer al menos, una letra y/o un número. Vuelva a ingresar.")
        return 1;
    }

    //Consulta si la cadena posee al menos, 5 líneas de extensión
    if(alias.length<=5)
    {
        alert("Alias debe poseer más de 5 caracteres. Número y letras obligatorio.")
        return 1;
    }
}
//Para validar rut tipo chileno (ejemplo: 1111111-1)
function revisaRut(rut)
{
    // Despejar Puntos
    var valor = rut.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7)
    {
        return 1; //Equivale a RUT inválido por ingreso
    }
    // Calcular dígito verificador
    suma = 0;
    multiplo = 2;
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++)
   {
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) 
        return 2; //nEquivale a RUT inválido por nv invalido
    $('#rut').val(cuerpo+"-"+dv);
    return; //Equivale a que el RUT es válido
}
//Validará el email
function revisaEmail(email)
{
    //Variable regex permitirá almacenar criterio de validación: nombre1@ejemplo.com.
    var regex = /^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

//Validando los checkbox. Mínimo 2 selecciones para pasar
function revisaCheckbox(cb1,cb2,cb3,cb4)
{
    var contador=0;
    if(cb1)
        contador++;
    if(cb2)
        contador++;
    if(cb3)
        contador++;
    if(cb4)
        contador++;
    //Los retornos dependerán si el contador es de 2 o más.
    if(contador>=2)
        return true;
    else
    {
        alert('Debe ingresar al menos dos opciones dentro de cómo se enteró de nosotros.')
        return false;
    }
}
/*FIN PARTE 1*/

/*PARTE 2-CAPTURA Y ENVÍO DE DATOS MEDIANTE AJAX*/
//Click en botón de votar
function operacion()
{
    //Captura de datos de textbox
    var nombre=document.getElementById('nombre').value;
	var alias=document.getElementById('alias').value;
    var rut=document.getElementById('rut').value;
    var email=document.getElementById('email').value;
    //Captura de combobox
    var region=$('#region').val();
    var ciudad=$('#ciudad').val();
    var candidato=$('#candidato').val();
    //Casillas de verificación:
    var web= $('#web').prop('checked');
    var TV= $('#TV').prop('checked');
    var rs= $('#rs').prop('checked');
    var amigo= $('#amigo').prop('checked');

    /*VALIDACIONES PREVIAS*/
    //Verificación campos vacíos de las capturas
	if(nombre =='' || alias =='' || rut =='' || email ==''|| region == 0 || ciudad==0 || candidato== 0)
	{
        alert('Debe completar los campos de este formulario');
        return;
    }

    //Verificación de los campos específicos: Alias, correo, rut

    if(!revisaEmail(email))
    {
        alert("Correo no válido. Favor corregir de forma nombre@ejemplo.com");
        return;
    }

    var checkrut=revisaRut(rut);
    if (checkrut ==2)
    {
        alert("RUT no válido. Favor corregir ingreso y/o dígito verificador de tipo 1111111-1");
        return;
    }
    else if(checkrut == 1)
    {
        alert("RUT no válido. Favor corregir a la forma tipo 1111111-1");
        return;
    }
    else
    {
        rut=$('#rut').val(); //Actualizar el valor del RUT para tener una tabla homologada
    }

    if(revisaAlias(alias) == 1)
        return;

    if(!revisaCheckbox(web,TV,rs,amigo))
        return;    

    /*FIN DE LAS VALIDACIONES */
    /*PREPARACIÓN DE AJAX*/
    $.ajax({
    type: "POST",
    url: "php/envio_voto.php",
    //Var del $_POST[''] : variable atrapada en JS
    data:{
        'nombre':nombre,
        'alias':alias,
        'rut':rut,
        'email':email,
        'region':region,
        'ciudad':ciudad,
        'candidato':candidato,
        'web':web,
        'TV':TV,
        'rs':rs,
        'amigo':amigo
        }
    })
    .done(function(valorSalida)
    {
        if(valorSalida == 0)
        {
            document.getElementById('divResultado').style.display="block";
            $("#labelResultado").html('Voto efectuado exitosamente. Recordar que solo se acepta un voto por persona.');
            reiniciar();
            alert('Voto efectuado exitosamente. Recordar que solo se acepta un voto por persona.');
        }
        else if(valorSalida == 1)
        {
            alert('El RUT en cuestión ya ha efectuado un voto. Solamente 1 voto por persona.');
        }
        else if(valorSalida == 2)
        {
            alert('Alias ya empleado por otra persona. Por favor, elija uno distinto.');
        }
        else if(valorSalida == 3)
        {
            alert('Error query');
        }
        else
        {
            alert(valorSalida)
        }
    })
    .fail(function()
    {
    alert('Hubo un errror al efectuar el voto. Por favor intentar más adelante.')
    })
}

/*FIN PARTE 2. CON ELLO, EL VOTO DEBERÁ ESTAR OK */
/*MÉTODO AUXILIAR PARA LIMPIAR OCURRENCIAS DEL FORMULARIO */

function reiniciar()
{
	document.getElementById('nombre').value='';
	document.getElementById('alias').value='';
    document.getElementById('rut').value='';
    document.getElementById('email').value='';
    document.getElementById('region').value=0;
    document.getElementById('ciudad').value="";
    document.getElementById('candidato').value = 0;
    $('#web').prop("checked", false);
    $('#TV').prop("checked", false);
    $('#rs').prop("checked", false);
    $('#amigo').prop("checked", false);
	$("#operacion").val(0);
}
