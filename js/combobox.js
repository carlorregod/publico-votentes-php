//Llenando los campos de combobox
$(document).ready(function()
{
    //Llamada 1 al AJAX para cargar el combobox de candidatos
    $.ajax({
        type: "POST",
        url: 'php/cargar_candidato.php'
        })
        .done(function(candidato){
        $('#candidato').html(candidato)
        })
        .fail(function(){
        alert('Hubo un errror al cargar los candidatos')
        })
    
    //Llamada 2 al AJAX para cargar el combobox de regiones
    $.ajax({
        type: "POST",
        url: 'php/cargar_region.php'
        })
        .done(function(region){
        $('#region').html(region)
        })
        .fail(function(){
        alert('Hubo un errror al cargar las regiones')
        })

    //Tercer AJAX, para complete del combobox si región fue elegida
    $('#region').on('change', function(){
        var id = $('#region').val()
        $.ajax({
        type: 'POST',
        url: 'php/cargar_ciudad.php',
        data: {'id': id}
        })
        .done(function(ciudad){
        $('#ciudad').html(ciudad)
        })
        .fail(function(){
        alert('Hubo un errror al cargar las ciudades')
        })
    })
})