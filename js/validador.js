function soloLetrasNombre(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "áéíóúabcdefghijklmnñopqrstuvwxyz ";  //notar que no dejará ingreso de espacios. Sólo caracteres
   //especiales = [8,37,39,46];

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
};

function ingresoCorreoElectronico(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "áéíóúabcdefghijklmnñopqrstuvwxyz@._-0123456789"; 

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
};

function soloNumerosLetras(e)
{
    //Sólo aceptará números positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^\w+$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
};

function soloNumerosRut(e)
{
    //Sólo aceptará números positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[0-9\-]+$/;//este acepta punto(-), si se quiere eliminar borrar el punto despues del 9. 
	te = String.fromCharCode(tecla); 
	return patron.test(te);
};